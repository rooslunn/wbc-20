<?php
declare(strict_types=1);

namespace RussForWbc\Controllers;

use Phalcon\Mvc\Controller;

class ControllerBase extends Controller
{
    protected function initialize()
    {
        $this->tag->prependTitle('RussForWbc | ');
        $this->view->setTemplateAfter('main');
    }
}
