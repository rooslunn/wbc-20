<?php
declare(strict_types=1);

namespace RussForWbc\Controllers;

class IndexController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();

        $this->tag->setTitle('Welcome');
    }

    public function indexAction(): void
    {
        $this->flash->notice(
            'This is test assignment to check my skills on how quickly
            I can get ready with Phalcon.');
    }
}
