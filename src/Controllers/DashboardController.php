<?php
declare(strict_types=1);


namespace RussForWbc\Controllers;

class DashboardController extends ControllerBase
{
    public function initialize()
    {
        $this->tag->setTitle('Dashboard');

        parent::initialize();
    }

    public function indexAction(): void
    {
    }

}
