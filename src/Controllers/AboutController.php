<?php
declare(strict_types=1);


namespace RussForWbc\Controllers;

class AboutController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();

        $this->tag->setTitle('About me');
    }

    public function indexAction(): void
    {
    }
}
