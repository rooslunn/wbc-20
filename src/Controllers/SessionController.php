<?php
declare(strict_types=1);

namespace RussForWbc\Controllers;

use RussForWbc\Models\Users;

class SessionController extends ControllerBase
{
    public function initialize()
    {
        parent::initialize();

        $this->tag->setTitle('Sign In');
    }

    public function indexAction(): void
    {
        $this->tag->setDefault('email', 'russ');
        $this->tag->setDefault('password', 'kladko');
    }

    /**
     * This action authenticate and logs an user into the application
     */
    public function startAction(): void
    {
        if ($this->request->isPost()) {
            $email = $this->request->getPost('email');
            $password = $this->request->getPost('password');

            /** @var Users $user */
//            $user = Users::findFirst([
//                "(email = :email: OR username = :email:) AND password = :password: AND active = 'Y'",
//                'bind' => [
//                    'email'    => $email,
//                    'password' => sha1($password),
//                ],
//            ]);

            // Users Stub for testing
            $user = new \stdClass();
            $user->id = 1;
            $user->name = 'Ruslan Kladko';

            if ($user) {
                $this->registerSession($user);
                $this->flash->success('Welcome ' . $user->name);

                $this->dispatcher->forward([
                    'controller' => 'dashboard',
                    'action'     => 'index',
                ]);

                return;
            }

            $this->flash->error('Wrong email/password');
        }

        $this->dispatcher->forward([
            'controller' => 'session',
            'action'     => 'index',
        ]);
    }

    /**
     * Finishes the active session redirecting to the index
     */
    public function endAction(): void
    {
        $this->session->remove('auth');
        $this->flash->success('Goodbye!');

        $this->dispatcher->forward([
            'controller' => 'index',
            'action'     => 'index',
        ]);
    }

    private function registerSession($user): void
    {
        $this->session->set('auth', [
            'id'   => $user->id,
            'name' => $user->name,
        ]);
    }

}
