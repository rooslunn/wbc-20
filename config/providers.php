<?php

return [
    \RussForWbc\Providers\ConfigProvider::class,
    \RussForWbc\Providers\DatabaseProvider::class,
    \RussForWbc\Providers\DispatcherProvider::class,
    \RussForWbc\Providers\FlashProvider::class,
    \RussForWbc\Providers\SessionProvider::class,
    \RussForWbc\Providers\SessionBagProvider::class,
    \RussForWbc\Providers\UrlProvider::class,
    \RussForWbc\Providers\ViewProvider::class,
    \RussForWbc\Providers\VoltProvider::class,
];
