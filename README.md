# Ruslan Kladko Test assignment 

### Requirements

* PHP >= 7.2
* [Apache][2] Web Server with [mod_rewrite][3] enabled or [Nginx][4] Web Server
* Latest stable [Phalcon Framework release][5] extension enabled
* [MySQL][6] >= 5.5

### Installation

1. Copy project to local environment - `git@gitlab.com:rooslunn/wbc-20.git`
2. Copy file `cp .env.example .env`
3. Edit .env file with your DB connection information
4. Run DB migrations `vendor/bin/phalcon-migrations migration run --config=migrations.php`

### Deployed on AWS

* [Russ.For.Wbc][7]

[1]: https://phalcon.io/
[2]: http://httpd.apache.org/
[3]: http://httpd.apache.org/docs/current/mod/mod_rewrite.html
[4]: http://nginx.org/
[5]: https://github.com/phalcon/cphalcon/releases
[6]: https://www.mysql.com/
[7]: http://3.9.188.208/
