<div class="page-header">
    <h2>About me</h2>
</div>

<p>
    20 years in IT, more than 10 years in Web Development.
    Experienced with all modern cutting-edge web technologies and methodologies.
    Strive for clean readable code, performance, and essential development principles: SOLID, DRY, YAGNI and 12factor.
    Cloud computing is a future for any web (and other) development.
    So AWS, GCP etc Rullez!
</p>
